A [CC0 waiver] is applied to all code in this repository.  You may do
whatever you want with the code without restriction.

  [CC0 waiver]: https://creativecommons.org/publicdomain/zero/1.0/