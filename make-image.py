#!/usr/bin/env python3

import datetime
import subprocess

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.font_manager
import numpy as np
import pandas as pd

# Borrowed from ColorBrewer
COLORS = pd.DataFrame({
    'red': (228,26,28),
    'blue': (55,126,184),
    'green': (77,175,74),
    'purple': (152,78,163),
    'orange': (255,127,0),
    'yellow': (255,255,51),
    'brown': (166,86,40),
    'pink': (247,129,191),
    'grey': (153,153,153),
}) / 255

PARTY_NAMES = pd.Series({
    1: 'Federalist',
    9: 'Jefferson Republican',
    10: 'Anti-Federalist',
    11: 'Jefferson Democrat',
    13: 'Democrat-Republican',
    22: 'Adams',
    25: 'National Republican',
    26: 'Anti Masonic',
    29: 'Whig',
    34: 'Whig and Democrat',
    37: 'Constitutional Unionist',
    40: 'Anti-Democrat and States Rights',
    41: 'Anti-Jackson Democrat',
    43: 'Calhoun Nullifier',
    44: 'Nullifier',
    46: 'States Rights',
    48: 'States Rights Whig',
    100: 'Democratic',
    101: 'Jackson Democrat',
    103: 'Democrat and Anti-Mason',
    104: 'Van Buren Democrat',
    105: 'Conservative Democrat',
    108: 'Anti-Lecompton Democrat',
    110: 'Popular Sovereignty Democrat',
    112: 'Conservative',
    114: 'Readjuster',
    117: 'Readjuster Democrat',
    118: 'Tariff for Revenue Democrat',
    119: 'United Democrat',
    200: 'Republican',
    202: 'Union Conservative',
    203: 'Unconditional Unionist',
    206: 'Unionist',
    208: 'Liberal Republican',
    212: 'United Republican',
    213: 'Progressive Republican',
    214: 'Non-Partisan and Republican',
    215: 'War Democrat',
    300: 'Free Soil',
    301: 'Free Soil Democrat',
    302: 'Free Soil Whig',
    304: 'Anti-Slavery',
    308: 'Free Soil American and Democrat',
    310: 'American',
    326: 'National Greenbacker',
    328: 'Independent',
    329: 'Ind. Democrat',
    331: 'Ind. Republican',
    333: 'Ind. Republican-Democrat',
    336: 'Anti-Monopolist',
    337: 'Anti-Monopoly Democrat',
    340: 'Populist',
    341: 'People\'s',
    347: 'Prohibitionist',
    353: 'Ind. Silver Republican',
    354: 'Silver Republican',
    355: 'Union',
    356: 'Union Labor',
    370: 'Progressive',
    380: 'Socialist',
    401: 'Fusionist',
    402: 'Liberal',
    403: 'Law and Order',
    522: 'American Labor',
    523: 'American Labor (La Guardia)',
    537: 'Farmer-Labor',
    555: 'Jackson',
    603: 'Ind. Whig',
    1060: 'Silver',
    1061: 'Emancipationist',
    1111: 'Liberty',
    1116: 'Conservative Republican',
    1275: 'Anti-Jackson',
    1346: 'Jackson Republican',
    3333: 'Opposition',
    3334: 'Opposition (36th Congress)',
    4000: 'Anti-Administration',
    4444: 'Union',
    5000: 'Pro-Administration',
    6000: 'Crawford Federalist',
    6666: 'Crawford Republican',
    7000: 'Jackson Federalist',
    7777: 'Crawford Republican',
    8000: 'Adams-Clay Federalist',
    8888: 'Adams-Clay Republican',
    9000: 'Unknown',
    9999: 'Unknown',
})

PARTY_COLORS = pd.Series({
     100: COLORS.blue,  # Democrat - blue
      13: COLORS.blue,  # Democrat-Republican - blue
     200: COLORS.red,  # Republican
      29: COLORS.purple,  # Whig
     555: COLORS.orange,  # Jackson
       1: COLORS.yellow,  # Federalist
    1275: COLORS.brown,  # Anti-Jackson
      22: COLORS.pink,  # Adams
    # Everyone else - grey
    5000: COLORS.grey,  # Pro-Administration
    3333: COLORS.grey,  # Opposition
    3334: COLORS.grey,  # Opposition (36th Congress)
    4000: COLORS.grey,  # Anti-Administration
    9: COLORS.grey,  # Jefferson Republican
    10: COLORS.grey,  # Anti-Federalist
    11: COLORS.grey,  # Jefferson Democrat
    25: COLORS.grey,  # National Republican
    26: COLORS.grey,  # Anti Masonic
    34: COLORS.grey,  # Whig and Democrat
    37: COLORS.grey,  # Constitutional Unionist
    40: COLORS.grey,  # Anti-Democrat and States Rights
    41: COLORS.grey,  # Anti-Jackson Democrat
    43: COLORS.grey,  # Calhoun Nullifier
    44: COLORS.grey,  # Nullifier
    46: COLORS.grey,  # States Rights
    48: COLORS.grey,  # States Rights Whig
    101: COLORS.grey,  # Jackson Democrat
    103: COLORS.grey,  # Democrat and Anti-Mason
    104: COLORS.grey,  # Van Buren Democrat
    105: COLORS.grey,  # Conservative Democrat
    108: COLORS.grey,  # Anti-Lecompton Democrat
    110: COLORS.grey,  # Popular Sovereignty Democrat
    112: COLORS.grey,  # Conservative
    114: COLORS.grey,  # Readjuster
    117: COLORS.grey,  # Readjuster Democrat
    118: COLORS.grey,  # Tariff for Revenue Democrat
    119: COLORS.grey,  # United Democrat
    202: COLORS.grey,  # Union Conservative
    203: COLORS.grey,  # Unconditional Unionist
    206: COLORS.grey,  # Unionist
    208: COLORS.grey,  # Liberal Republican
    212: COLORS.grey,  # United Republican
    213: COLORS.grey,  # Progressive Republican
    214: COLORS.grey,  # Non-Partisan and Republican
    215: COLORS.grey,  # War Democrat
    300: COLORS.grey,  # Free Soil
    301: COLORS.grey,  # Free Soil Democrat
    302: COLORS.grey,  # Free Soil Whig
    304: COLORS.grey,  # Anti-Slavery
    308: COLORS.grey,  # Free Soil American and Democrat
    310: COLORS.grey,  # American
    326: COLORS.grey,  # National Greenbacker
    328: COLORS.grey,  # Independent 
    329: COLORS.grey,  # Ind. Democrat
    331: COLORS.grey,  # Ind. Republican
    333: COLORS.grey,  # Ind. Republican-Democrat
    336: COLORS.grey,  # Anti-Monopolist
    337: COLORS.grey,  # Anti-Monopoly Democrat
    340: COLORS.grey,  # Populist
    341: COLORS.grey,  # People's
    347: COLORS.grey,  # Prohibitionist
    353: COLORS.grey,  # Ind. Silver Republican
    354: COLORS.grey,  # Silver Republican
    355: COLORS.grey,  # Union 
    356: COLORS.grey,  # Union Labor
    370: COLORS.grey,  # Progressive
    380: COLORS.grey,  # Socialist
    401: COLORS.grey,  # Fusionist
    402: COLORS.grey,  # Liberal
    403: COLORS.grey,  # Law and Order
    522: COLORS.grey,  # American Labor
    523: COLORS.grey,  # American Labor (La Guardia)
    537: COLORS.grey,  # Farmer-Labor
    603: COLORS.grey,  # Ind. Whig
    1060: COLORS.grey,  # Silver
    1061: COLORS.grey,  # Emancipationist 
    1111: COLORS.grey,  # Liberty
    1116: COLORS.grey,  # Conservative Republican
    1346: COLORS.grey,  # Jackson Republican
    4444: COLORS.grey,  # Union
    6000: COLORS.grey,  # Crawford Federalist
    6666: COLORS.grey,  # Crawford Republican
    7000: COLORS.grey,  # Jackson Federalist
    7777: COLORS.grey,  # Crawford Republican
    8000: COLORS.grey,  # Adams-Clay Federalist
    8888: COLORS.grey,  # Adams-Clay Republican     
    9000: COLORS.grey,  # Unknown
    9999: COLORS.grey,  # Unknown
})

REGION_COLORS = pd.Series({
    # Northeast - blue
    1: COLORS.blue,
    2: COLORS.blue,
    3: COLORS.blue,
    4: COLORS.blue,
    5: COLORS.blue,
    6: COLORS.blue,
    12: COLORS.blue,
    13: COLORS.blue,
    14: COLORS.blue,
    # Midwest - orange
    21: COLORS.orange,
    22: COLORS.orange,
    23: COLORS.orange,
    24: COLORS.orange,
    25: COLORS.orange,
    31: COLORS.orange,
    32: COLORS.orange,
    33: COLORS.orange,
    34: COLORS.orange,
    35: COLORS.orange,
    36: COLORS.orange,
    37: COLORS.orange,
    # South - red
    11: COLORS.red,
    43: COLORS.red,
    44: COLORS.red,
    52: COLORS.red,
    47: COLORS.red,
    48: COLORS.red,
    40: COLORS.red,
    55: COLORS.red,
    56: COLORS.red,
    41: COLORS.red,
    51: COLORS.red,
    46: COLORS.red,
    54: COLORS.red,
    42: COLORS.red,
    45: COLORS.red,
    53: COLORS.red,
    49: COLORS.red,
    # West - green
    61: COLORS.green,
    62: COLORS.green,
    63: COLORS.green,
    64: COLORS.green,
    65: COLORS.green,
    66: COLORS.green,
    67: COLORS.green,
    68: COLORS.green,
    81: COLORS.green,
    71: COLORS.green,
    82: COLORS.green,
    72: COLORS.green,
    73: COLORS.green,
    # Other - grey
    99: COLORS.grey,
})

MARKER_WIDTH = 1
MARKER_COLOR = 'grey'
MARKER_STYLE = ':'
PERSON_WEIGHT = 20
TIMESPAN_COLOR = [0.9] * 3

def year_fraction(year, month, day):
    """Takes a date and returns the date as a decimal year."""
    d = datetime.date(year, month, day)
    year_start = datetime.date(year, 1, 1)
    year_days = (datetime.date(year + 1, 1, 1) - year_start).days
    elapsed_days = (d - year_start).days
    return year + elapsed_days / year_days

def add_tick(x, label):
    ax = plt.gca()
    xt = ax.get_xticks().tolist() + [x]
    xtl = ax.get_xticklabels() + [label]
    ax.set_xticks(xt)
    ax.set_xticklabels(xtl)
    
def plot_marker(year, month, day, description, line=True):
    x = year_fraction(year, month, day)
    if line:
        plt.axvline(x, linewidth=MARKER_WIDTH, color=MARKER_COLOR, linestyle=MARKER_STYLE, zorder=0)
    add_tick(x, description)

def plot_timespan(y1, m1, d1, y2, m2, d2, description):
    x1 = year_fraction(y1, m1, d1)
    x2 = year_fraction(y2, m2, d2)
    plt.axvspan(x1, x2, facecolor=TIMESPAN_COLOR, zorder=0)
    add_tick((x1 + x2) / 2, description)

def add_ticks(df):
    plt.gca().set_xticks([])
    plot_marker(1789, 1, 1, '1789   ', False)
    plot_marker(1795, 1, 1, '   1795', False)
    plot_marker(1825, 1, 1, '1825', False)
    plot_marker(1837, 1, 1, '1837', False)
    plot_timespan(1861, 4, 12, 1865, 5, 9, 'Civil War')
    plot_timespan(1929, 10, 24, 1933, 12, 31, 'Great Depression')
    plot_marker(1964, 7, 2, 'Civil Rights Act   \nof 1964   ')
    plot_marker(1980, 11, 4, '   Reagan\n   Elected')
    last_congress = df.congress.max()
    last_year = 1787 + last_congress * 2
    plot_marker(last_year, 1, 1, str(last_year), False)
    
    patches = [
        mpatches.Patch(color=COLORS.blue, label='Democratic'),
        mpatches.Patch(color=COLORS.red, label='Republican'),
        mpatches.Patch(color=COLORS.purple, label='Whig'),
        mpatches.Patch(color=COLORS.orange, label='Jackson'),
        mpatches.Patch(color=COLORS.yellow, label='Federalist'),
        mpatches.Patch(color=COLORS.brown, label='Anti-Jackson'),
        mpatches.Patch(color=COLORS.pink, label='Adams'),
        mpatches.Patch(color=COLORS.grey, label='Other'),
    ]

    plt.legend(handles=patches, title='Party', ncol=3)

    plt.text(
        0.005, 0.01,
        'One dot per representative. Color intensity scaled by total number of representatives.',
        transform=plt.gca().transAxes)
    plt.figtext(
        0.995, 0.01,
        'Data from https://voteview.com/data',
        horizontalalignment='right', fontsize='small')
    
def plot_data(df, description, basename):
    # Set a reasonable-looking font
    available_fonts = set(matplotlib.font_manager.get_font_names())
    for font in ['Work Sans', 'sans-serif']:
        if font in available_fonts:
            plt.rcParams['font.family'] = font
            break

    # Fix origin data.  "Republican" (200) before 1853 (33rd Congress)
    # should be "Democratic-Republican" (13)
    df.loc[(df.congress < 33) & (df.party_code == 200), 'party'] = 13
    
    congressional_population = df.congress.value_counts()
    person_color = [list(PARTY_COLORS[row.party_code]) + [PERSON_WEIGHT/congressional_population[row.congress]] for row in df.itertuples()]

    
    ## First figure: liberal-conservative
    
    fig = plt.figure(figsize=(12.80, 7.20))
    ax = fig.add_axes((0.07, 0.1, 0.9, 0.84))
    
    plt.scatter(df.congress * 2 + 1787, df.nominate_dim1, c=person_color, marker='o', linewidths=0, edgecolors='none')
    plt.xlim(df.congress.min() * 2 + 1787 - 1, df.congress.max() * 2 + 1787 + 1)

    add_ticks(df)

    plt.title('US {} Party Affiliation and Political Alignment'.format(description))
    plt.ylabel('NOMINATE economic liberal-conservative score')
    
    plt.savefig('{}.png'.format(basename))
    plt.close(fig)


    ## Second figure: regional/social
    
    fig = plt.figure(figsize=(12.80, 7.20))
    ax = fig.add_axes((0.07, 0.1, 0.9, 0.84))
    
    plt.scatter(df.congress * 2 + 1787, df.nominate_dim2, c=person_color, marker='o', linewidths=0, edgecolors='none')
    plt.xlim(df.congress.min() * 2 + 1787 - 1, df.congress.max() * 2 + 1787 + 1)

    add_ticks(df)

    plt.title('US {} Party Affiliation and Political Alignment'.format(description))
    plt.ylabel('NOMINATE regional/social score')
    
    plt.savefig('{}-social.png'.format(basename))
    plt.close(fig)
    

    ## Let's try highlighting a state
    
    person_color = [list(PARTY_COLORS[row.party_code]) + [PERSON_WEIGHT/10/congressional_population[row.congress]] for row in df.itertuples()]

    fig = plt.figure(figsize=(12.80, 7.20))
    ax = fig.add_axes((0.07, 0.1, 0.9, 0.84))
    
    plt.scatter(df.congress * 2 + 1787, df.nominate_dim1, c=person_color, marker='o', linewidths=0, edgecolors='none')
    plt.xlim(df.congress.min() * 2 + 1787 - 1, df.congress.max() * 2 + 1787 + 1)

    state = 52  # MD
    state_df = df[df.state_abbrev == state]
    state_population = state_df.congress.value_counts()
    person_color = [PARTY_COLORS[row.party_code].append(pd.Series([min(1, 2/state_population[row.cong])], index=[3])) for row in state_df.itertuples()]

    plt.scatter(state_df.congress * 2 + 1787, state_df.nominate_dim1, c=person_color, marker='o', linewidths=0, edgecolors='none')
        
    add_ticks(df)

    plt.title('US {} Party Affiliation and Political Alignment'.format(description))
    plt.ylabel('NOMINATE economic liberal-conservative score')
    
    plt.savefig('{}-MD.png'.format(basename))
    plt.close(fig)


#plot_data(pd.read_csv('HSall_members.csv'), 'Congress', 'congress')
plot_data(pd.read_csv('Hall_members.csv'), 'House of Representatives', 'house')
plot_data(pd.read_csv('Sall_members.csv'), 'Senate', 'senate')
